/*
 * Copyright (C) 2014 Sony Mobile Communications AB.
 * All rights, including trade secret rights, reserved.
 */
 
#include "../cfgfileinc/CFG_Custom1_File.h"
#include "../cfgfileinc/CFG_GPS_File.h"
#include "../cfgfileinc/CFG_PRODUCT_INFO_File.h"
#include "../cfgfileinc/CFG_Wifi_File.h"
//CEI comments start //
//LE:A Add NVRAM
#include "../cfgfileinc/CFG_SW_VER_File.h"
#include "../cfgfileinc/CFG_PROJ_ID_File.h"
//CEI comments end //
#include "../cfgfileinc/CFG_P_SENSOR_THRESHOLD_File.h"
